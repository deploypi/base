{% if pillar.plycloud.file_server.samba is defined %}
{% if pillar.plycloud.file_server.samba.run == true %}

samba:
  pkg:
    - installed
  service:
    - running
    - name: smbd
    - enable: True
    - require:
      - pkg: samba
    - watch:
      - file: /etc/samba/smb.conf

samba_conf:
  file.managed:
    - name : /etc/samba/smb.conf
    - template: jinja
    - source: salt://plycloud/file_server/samba/files/smb.conf.jinja
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: samba

{% for samba_share, details in pillar.plycloud.file_server.samba.shares.iteritems() %}
{{ details.path }}:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: True
{% endfor %}

{% endif %}
{% endif %}