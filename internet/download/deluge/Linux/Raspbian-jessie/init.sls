{% if pillar.plycloud.internet.download.deluged is defined %}
{% if pillar.plycloud.internet.download.deluged.run == true %}

deluge_pkgs:
  pkg.installed:
    - pkgs:
      - deluged
      - deluge-console
      - deluge-webui

deluged_conf:
  file.replace:
    - name: /etc/default/deluged
    - pattern: ENABLE_DELUGED=0
    - repl: ENABLE_DELUGED=1

deluged_init:
  file.replace:
    - name: /etc/init.d/deluged
    - pattern: MASK=0027
    - repl: MASK=0000
  cmd.run:
    - names:
      - update-rc.d deluged defaults
      - systemctl daemon-reload
    - onchanges:
      - file: /etc/default/deluged

deluged_service:
  service.running:
    - name: deluged
    - enable: True
    - watch:
      - file: /etc/default/deluged
      - file: /etc/init.d/deluged
    - require:
      - pkg: deluge_pkgs

deluge_web_conf:
  file.managed:
    - name: /etc/default/deluge-web
    - template: jinja
    - source: salt://plycloud/internet/download/deluge/files/deluge-web.conf.jinja
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: deluge_pkgs

deluge_web_init:
  file.managed:
    - name: /etc/init.d/deluge-web
    - template: jinja
    - source: salt://plycloud/internet/download/deluge/files/deluge-web.init.jinja
    - mode: 755
    - user: root
    - group: root
    - require:
      - pkg: deluge_pkgs
      - file: /etc/default/deluge-web

deluge_web_service_register:
  cmd.run:
    - name: update-rc.d deluge-web defaults
    - onchanges:
      - file: /etc/default/deluge-web
      - file: /etc/init.d/deluge-web

deluge_web_service:
  service.running:
    - name: deluge-web
    - enable: True
    - watch:
      - file: /etc/default/deluge-web
      - file: /etc/init.d/deluge-web
      - file: /etc/default/deluged
      - file: /etc/init.d/deluged
    - require:
      - pkg: deluge_pkgs
      - file: /etc/init.d/deluge-web

{{ pillar.plycloud.internet.download.deluged.download_dir }}:
  file.directory:
    - makedirs: True
    - user: debian-deluged
    - group: debian-deluged
    - mode: 777
    - file_mode: 644
    - recurse:
      - user
      - group
      - mode

deluge_default_conf:
  cmd.run:
    - names:
      - deluge-console -c /var/lib/deluged/config/ config --set download_location {{ pillar.plycloud.internet.download.deluged.download_dir }}/downloading
      - deluge-console -c /var/lib/deluged/config/ config --set torrentfiles_location {{ pillar.plycloud.internet.download.deluged.download_dir }}
      - deluge-console -c /var/lib/deluged/config/ config --set move_completed_path {{ pillar.plycloud.internet.download.deluged.download_dir }}
      - deluge-console -c /var/lib/deluged/config/ config --set autoadd_location {{ pillar.plycloud.internet.download.deluged.download_dir }}/auto_add
      - deluge-console -c /var/lib/deluged/config/ config --set autoadd_enable True
      - deluge-console -c /var/lib/deluged/config/ config --set move_completed True
      - deluge-console -c /var/lib/deluged/config/ config --set copy_torrent_file True
      - deluge-console -c /var/lib/deluged/config/ config --set prioritize_first_last_pieces True
    - onchanges:
      - file: /etc/init.d/deluged
      - file: /etc/default/deluged
      - service: deluged_service
    - require:
      - service: deluged_service

{% endif %}
{% endif %}