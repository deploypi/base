{% if pillar.plycloud.media_server.plex is defined %}
{% if pillar.plycloud.media_server.plex.run == true %}

media_server_plex:
  pkgrepo.managed:
    - humanname: plexmediaserver
    - name: deb https://dev2day.de/pms/ jessie main
    - file: /etc/apt/sources.list.d/pms.list
    - key_url: https://dev2day.de/pms/dev2day-pms.gpg.key
  pkg.installed:
    - pkgs:
      - apt-transport-https
      - plexmediaserver
    - require:
      - pkgrepo: media_server_plex
  service:
    - running
    - name: plexmediaserver
    - reload: True
    - enable: True
    - require:
      - pkg: media_server_plex


{% endif %}
{% endif %}