/etc/profile.d/deploypi_bash_profile.sh:
  file.managed:
    - template: jinja
    - source: salt://plycloud/os/base/files/deploypi_bash_profile.jinja
    - mode: 644
    - user: root
    - group: root

/etc/skel/.bash_aliases:
  file.managed:
    - template: jinja
    - source: salt://plycloud/os/base/files/deploypi_bash_profile.jinja
    - mode: 644
    - user: root
    - group: root

/root/.bash_aliases:
  file.managed:
    - template: jinja
    - source: salt://plycloud/os/base/files/deploypi_bash_profile.jinja
    - mode: 644
    - user: root
    - group: root

/home/pi/.bash_aliases:
  file.managed:
    - template: jinja
    - source: salt://plycloud/os/base/files/deploypi_bash_profile.jinja
    - mode: 644
    - user: pi
    - group: pi
