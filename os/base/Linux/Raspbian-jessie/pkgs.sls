base_pkgs:
  pkg.installed:
    - pkgs:
      - curl
      - git-core
      - lftp
      - mc
      - ntp
      - rsync
      - tcpdump
      - wget
      - python-software-properties
      - lsb-core
    - refresh: True
