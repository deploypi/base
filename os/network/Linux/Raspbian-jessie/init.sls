{% if pillar.plycloud.os.network is defined %}
{% if pillar.plycloud.os.network.run == true %}

{% set hostname = pillar.plycloud.os.network.system.hostname %}
{% set domain = pillar.plycloud.os.network.system.domain %}

system:
  network.system:
    - enabled: True
    - hostname: {{ hostname }}
    - apply_hostname: True
    - nozeroconf: True
    - nisdomain: {{ domain }}

localhost:
  host.present:
    - ip:
      - 127.0.0.1
    - names:
      - localhost
      - localhost.localdomain
      - {{ hostname }}
      - {{ hostname }}.{{ domain }}

{% if pillar.plycloud.os.network.system.interface is defined %}
{{ hostname }}:
  host.present:
    - ip:
      {% for interface, details in pillar.plycloud.os.network.system.interface.iteritems() %}
      - {{ details.ip }}
      {% endfor %}
    - names:
      - {{ hostname }}
      - {{ hostname }}.{{ domain }}

dhcpcd5:
  pkg.installed:
    - name: dhcpcd5
  service.running:
    - name: dhcpcd
    - enable: True

dhcpcd_conf:
  file.managed:
    - name: /etc/dhcpcd.conf
    - source: salt://plycloud/os/network/{{ grains.kernel }}/{{ grains.os }}-{{ grains.oscodename }}/files/dhcpcd.conf.jinja
    - template: jinja
    - watch_in:
      - service: dhcpcd
{% endif %}
{% endif %}
{% endif %}