{% if pillar.plycloud.os.upgrade is defined %}
{% if pillar.plycloud.os.upgrade.run == true %}

os_python-software-properties:
  pkg.latest:
    - name: python-software-properties
    - refresh: True

os_distro-info:
  pkg.latest:
    - name: distro-info
    - refresh: True

os-upgrade:
  pkgrepo.managed:
    - humanname: Raspbian stretch
    - name: deb http://archive.raspbian.org/raspbian/ stretch main contrib non-free rpi
    - dist: stretch
    - file: /etc/apt/sources.list.d/raspbian-stretch.list
    - require:
      - pkg: python-software-properties

/usr/share/distro-info/debian.csv:
  file.copy:
    - name: raspbian.csv
    - source: /usr/share/distro-info/

os_update_packages:
  module.run:
    - name: pkg.upgrade
    - refresh: True

{% endif %}
{% endif %}