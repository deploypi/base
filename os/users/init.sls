{% if pillar.plycloud.os.users is defined %}
{% if pillar.plycloud.os.users.run == true %}
{% for username, details in pillar.plycloud.os.users.data.iteritems() %}
os_{{ username }}:
  group:
    - present
    - name: {{ username }}
    {% if 'gid' in details %}
    - gid: {{ details.gid }}
    {% endif %}
  user:
    - present
    {% if 'fullname' in details %}
    - fullname: {{ details.fullname }}
    {% endif %}
    - name: {{ username }}
    {% if 'shell' in details %}
    - shell: {{ details.shell }}
    {% endif %}
    {% if 'home' in details %}
    - home: {{ details.home }}/{{ username }}
    {% endif %}
    {% if 'uid' in details %}
    - uid: {{ details.uid }}
    {% endif %}
    - gid_from_name: true
    {% if 'password' in details %}
    - password: {{ details.password }}
    {% endif %}
    {% if 'groups' in details %}
    - remove_groups: false
    - optional_groups:
      {% for group in details.get('groups', []) %}
      - {{ group }}
      {% endfor %}
    {% endif %}

  {% if 'pub_ssh_keys' in details %}
  ssh_auth:
    - present
    - user: {{ username }}
    - names:
    {% for pub_ssh_key in details.get('pub_ssh_keys', []) %}
      - {{ pub_ssh_key }}
    {% endfor %}
    - require:
      - user: os_{{ username }}
  {% endif %}

{% endfor %}
{% endif %}
{% endif %}
