# Pillar must looks like:

os_users:
  demo1:
    fullname: Demo User
    uid: 1331
    gid: 1331
    groups:
      - sudo
      - gpio
    password: $6$QJ9NEXSr$Q4aO6yfyNKHImAkDpOMU8w36I3mgcudwIsiSpZBM2BCvvBDEActPShQaqZXCc7/03O6LuV2c7c8RfUvBL/tyU/
    pub_ssh_keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAgEAyBI0bIQ................K9uzbUwz2/t9cbU= Demo key 1
      - ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQB3PBvcD8b................tgdfgdfg2/t9cbU= Demo key 2

# Parameters like groups, uid, gid, password and pub_ssh_keys are optional
